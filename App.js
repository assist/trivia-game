import React from 'react';

import AppRouter from 'TriviaGame/TriviaGame/Views/AppRouter'

export default class App extends React.Component {
    render() {
        return (
            <AppRouter/>
        );
    }
}
