import WebService from "TriviaGame/TriviaGame/Helpers/WebService"

let ws = WebService.sharedWebService();

export default class SFData {
    constructor() {
        this.serviceID = null;
        this.data = {};
        this.isMandatory = false;
        this.delegate = null;
    }

    loadDataFromServer(get, post, success, failure) {
        if (this.isMandatory && this.delegate) this.delegate.showActivityView();

        ws.getFromServer(this.serviceID, get, post).then((function (json) {
            if (json && json.response_code == 0) {
                this.data = json;
                return this.data;
            } else if (failure) {
                failure(this.serviceID);
            }
        }).bind(this)).then((() => {
            if (success) success(this.serviceID);
            if (this.delegate) this.delegate.hideActivityView();
        }).bind(this));
    }
}