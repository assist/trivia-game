import MiscFunc from "./MiscFunc";


export default class WebService {
    static sharedInstance = null;

    constructor() {
        this.baseUrl = "https://opentdb.com/";
        this.defaultHeaders = {
            'Accept': 'application/json',
        }
    }

    static sharedWebService() {
        if (this.sharedInstance == null) {
            this.sharedInstance = new WebService();
        }
        return WebService.sharedInstance;
    }

    getFromServer (serviceID, get, post) {
        let url = this.baseUrl + serviceID;
        if (get) url += "?" + get;

        return fetch(url, {
            method: "GET",
            headers: this.defaultHeaders,
        }).then(response => {
            if (response.ok)
                return response.json();
            else
                throw "Server error";
        }).then(json => {
            return json;
        }).catch(error => {
            MiscFunc.debugLog("getFromServer error: " + error);
        })
    }
}

