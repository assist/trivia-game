const React = require("react-native");
const { Dimensions } = React;
const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;

export default {
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'space-around',
        paddingLeft: 20,
        paddingRight: 20,
        alignSelf: "center"
    },
    title: {
        textAlign: 'center',
        backgroundColor: 'transparent',
        fontSize: 20,
        fontWeight: 'bold',
        alignSelf: 'center'
    },
    score: {
        backgroundColor: 'transparent',
        fontSize: 16,
        fontWeight: 'bold',
    },
    text: {
        textAlign: 'center',
        backgroundColor: 'transparent',
        fontSize: 20,
        alignSelf: 'center'
    },
    smallText: {
        textAlign: 'center',
        backgroundColor: 'transparent',
        fontSize: 12,
        alignSelf: 'center'
    },
    questionBox: {
        width: deviceWidth*0.9,
        flex: 0.8,
        borderWidth: 1,
        borderColor: "black"
    },
    row: {
        alignSelf: "stretch",
        alignItems: "stretch",
        flexDirection: "row",
        justifyContent: "space-between",
    },
    list: {
        height: deviceHeight*0.7,
        width:deviceWidth*0.9
    }

}
;