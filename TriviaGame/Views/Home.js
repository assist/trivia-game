import React from "react";
import { Text, View, Button } from 'react-native';

import styles from "TriviaGame/TriviaGame/Resources/styles";
import AppScreenBase from 'TriviaGame/TriviaGame/Views/AppScreenBase'

export default class Home extends AppScreenBase {
    render() {
        return (
            <View style={styles.container} ref="root">
                {super.defaultView()}
                <Text style={styles.title}>Welcome to the Trivia Challenge!</Text>
                <Text style={styles.text}>You will be presented with 10 True of False questions.</Text>
                <Text style={styles.text}>Can you score 100%</Text>
                <Button title="BEGIN" color="black" onPress={() => this.navigateTo("Game")}/>
            </View>
        );
    }
}
