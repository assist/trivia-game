import React from "react";
import { Text, View, FlatList, Button } from 'react-native';
import MiscFunc from "TriviaGame/TriviaGame/Helpers/MiscFunc";

import styles from "TriviaGame/TriviaGame/Resources/styles";
import AppScreenBase from 'TriviaGame/TriviaGame/Views/AppScreenBase'

export default class Score extends AppScreenBase {
    render() {
        var score = 0;
        this.propsObject().questions.results.forEach(question => {
            let correctAnswer = (question.correct_answer == "True");
            if (question.userAnswer == correctAnswer)
                score++;
        });
        return (
            <View style={styles.container} ref="root">
                <Text style={styles.title}>{"You Scored\n" + score + " of 10"}</Text>
                <View style={styles.list}>
                    <FlatList style={styles.list} ref="list" data={this.propsObject().questions.results} renderItem={this.renderItem}/>
                </View>
                <Button title="PLAY AGAIN?" color="black" onPress={() => this.popToRoot()}/>
            </View>
        );
    }

    renderItem(data) {
        let correctAnswer = (data.item.correct_answer == "True");
        var answerColor = "green";
        var answerSign = "+"
        if (data.item.userAnswer != correctAnswer) {
            answerColor = "red";
            answerSign = "-";
        }

        return (
            <View style={[styles.row, {marginBottom: 10}]}>
                <Text style={[styles.score, {color: answerColor, flex: 0.1}]}>{answerSign}</Text>
                <Text style={[styles.score, {color: answerColor, flex: 0.9}]}>{MiscFunc.stripTags(data.item.question)}</Text>
            </View>
        )
    }

}
