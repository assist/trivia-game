import React from 'react';
import {StackNavigator} from 'react-navigation';

import Home from 'TriviaGame/TriviaGame/Views/Home'
import Game from 'TriviaGame/TriviaGame/Views/Game'
import Score from 'TriviaGame/TriviaGame/Views/Score'

const MainRouter = StackNavigator({
        Home: {screen: Home},
        Game: {screen: Game},
        Score: {screen: Score},
    },
    {

        index: 0,
        initialRouteName: "Home",
        headerMode: "none",
        cardStyle: {backgroundColor: "white"}
    }
);

export default () => <MainRouter /> ;