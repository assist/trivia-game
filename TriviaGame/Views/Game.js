import React from "react";
import { Text, View, Button } from 'react-native';

import styles from "TriviaGame/TriviaGame/Resources/styles";
import AppScreenBase from 'TriviaGame/TriviaGame/Views/AppScreenBase'
import MiscFunc from "TriviaGame/TriviaGame/Helpers/MiscFunc";
import SFData from "TriviaGame/TriviaGame/Helpers/SFData";

export default class Game extends AppScreenBase {
    constructor(props) {
        super(props);

        this.initState({questionId: 0});

        this.questions = new SFData();
        this.questions.serviceID = "api.php";
        this.questions.isMandatory = true;
        this.questions.delegate = this;
    }

    componentWillMount() {
        this.initData();
    }

    initData() {
        this.questions.loadDataFromServer("amount=10&difficulty=hard&type=boolean", null, this.onDownloadSuccess.bind(this));
    }

    answerQuestion(answer) {
        this.questions.data.results[this.state.questionId-1].key = this.state.questionId;
        this.questions.data.results[this.state.questionId-1].userAnswer = answer;
        if (this.state.questionId < 10)
            this.setState({questionId: this.state.questionId+1});
        else
            this.navigateTo("Score", {questions: this.questions.data});
    }

    render() {
        if (this.state.questionId)
            return (
                <View style={styles.container} ref="root">
                    {super.defaultView()}

                    <Text style={[styles.title, {height: 100}]}>{MiscFunc.stripTags(this.questions.data.results[this.state.questionId-1].category)}</Text>

                    <View style={{flex: 0.65}}>
                        <View style={[styles.container, styles.questionBox]} ref="root">
                            <Text style={styles.text}>{MiscFunc.stripTags(this.questions.data.results[this.state.questionId-1].question)}</Text>
                        </View>
                        <Text style={[styles.smallText, {marginTop: 10}]}>{this.state.questionId + " of 10"}</Text>
                    </View>

                    <View style={styles.row}>
                        <Button title="WRONG" color="red" onPress={() => this.answerQuestion(false)}/>
                        <Button title="RIGHT" color="green" onPress={() => this.answerQuestion(true)}/>
                    </View>
                </View>
            );
        else
            return super.defaultView();
    }

    onDownloadSuccess(serviceID) {
        if (serviceID == this.questions.serviceID) {
            MiscFunc.debugLog(this.questions.data);
            this.setState({questionId: 1});
        }
    }
}
