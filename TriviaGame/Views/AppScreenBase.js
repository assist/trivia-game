import React from 'react'
import Spinner from 'react-native-loading-spinner-overlay';
import MiscFunc from "TriviaGame/TriviaGame/Helpers/MiscFunc";


export default class AppScreenBase extends React.Component {
    constructor(props) {
        super(props);

        this.state = {showActivityIndicator: 0};
    }

    initState(stateMap) {
        this.state = MiscFunc.mergeDeep(this.state, stateMap);
    }

    propsObject() {
        return this.props.navigation.state.params;
    }

    defaultView() {
        return (
            <Spinner visible={this.state.showActivityIndicator != 0} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
        );
    }

    showActivityView() {
        this.setState({showActivityIndicator: this.state.showActivityIndicator+1});
    }

    hideActivityView() {
        this.setState({showActivityIndicator: Math.max(this.state.showActivityIndicator-1, 0)});
    }

    navigationObject() {
        return (this.props.screenProps && this.props.screenProps.navigation) || this.props.navigation;
    }

    navigateTo(screenName, props) {
        const { navigate } = this.props.navigation;
        navigate(screenName, props);
    }

    navigateBack() {
        const { goBack } = this.props.navigation;
        goBack();
    }

    popToRoot() {
        this.resetNavigationTo("Home", null);
    }

    resetNavigationTo(screenName, props) {
        this.navigationObject().dispatch({type: 'Navigation/RESET', index: 0, actions: [{ type: 'Navigation/Navigate', routeName:screenName, params: props}]})
    }

}
